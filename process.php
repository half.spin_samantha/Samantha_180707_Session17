<?php


if(isset($_POST["StudentID"]))
{
    echo "<h1> Showing Student Information </h1>";
    $objStudent = new Student();

    $objStudent->setData($_POST);
    $objStudent->showThisInfo();
    $objStudent->writeToFile();

}

if(!isset($_POST["StudentID"]) && isset($_POST))
{
    echo "<h1> Showing Person Information </h1>";
    $objPerson = new Person();

    $objPerson->setData($_POST);
    $objPerson->showThisInfo();
    $objPerson->writeToFile();


}



class Person
{

    protected $name;
    protected $dob;
    public $gender;

    public function writeToFile(){
        file_put_contents("person.txt",$this->name."\n",FILE_APPEND);
        file_put_contents("person.txt",$this->dob."\n",FILE_APPEND);
        file_put_contents("person.txt",$this->gender."\n",FILE_APPEND);
    }

    public function setData($posTArray){


        if(array_key_exists("Name",$posTArray));
        $this->name = $posTArray["Name"];

        if(array_key_exists("DOB",$posTArray));
        $this->dob = $posTArray["DOB"];

        if(array_key_exists("Gender",$posTArray));
        $this->gender = $posTArray["Gender"];
    }

   /* public function getName()
    {
        return $this->name;
    }

    public function getdob()
    {
        return $this->dob;
    }

    public function getgender()
    {
        return $this->gender;
    }*/

    public function showThisInfo()
    {
        echo $this->getName()."<br>";
        echo $this->getdob()."<br>";
        echo $this->getgender()."<br>";
    }


}

/*
$objPerson = new Person();

$objPerson->setName("Samantha<br>");
$objPerson->setdob("23-09-1995<br>");
$objPerson->setgender("Female<br>");

echo $objPerson->getName();
echo $objPerson->getdob();
echo $objPerson->getgender();
*/


class Student extends Person{

        protected  $studentid;

    public function getstudentid()
    {
        return $this->studentid;
    }

    public function writeToFile(){

        file_put_contents("person.txt",$this->studentid."\n",FILE_APPEND);
        file_put_contents("person.txt",$this->name."\n",FILE_APPEND);
        file_put_contents("person.txt",$this->dob."\n",FILE_APPEND);
        file_put_contents("person.txt",$this->gender."\n",FILE_APPEND);
    }


    public function setData($posTArray){

        if(array_key_exists("StudentID",$posTArray));
        $this->studentid = $posTArray["StudentID"];
        
        if(array_key_exists("Name",$posTArray));
        $this->name = $posTArray["Name"];

        if(array_key_exists("DOB",$posTArray));
        $this->dob = $posTArray["DOB"];

        if(array_key_exists("Gender",$posTArray));
        $this->gender = $posTArray["Gender"];
    }

    public function showAllData()
    {
        echo $this->getName()."<br>";
        echo $this->getdob()."<br>";
        echo $this->getgender()."<br>";
        echo $this->getstudentid()."<br>";
    }


}

?>